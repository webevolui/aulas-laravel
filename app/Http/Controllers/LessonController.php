<?php

namespace App\Http\Controllers;

use App\Models\Lesson;
use App\Models\User;
use App\Notifications\LessonAdded;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Lesson::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lesson = \App\Models\Lesson::factory()->create();

        Notification::send(User::all(), new LessonAdded($lesson));

        return $lesson;
    }
}
