<?php 

require_once '../vendor/autoload.php';
// require_once "../conexao.php";

// Incluindo a classe que criamos
require_once 'BackupDatabase.php';

// Como a geração do backup pode ser demorada, retiramos
// o limite de execução do script
set_time_limit(0);

if (!file_exists('backups')) {
    mkdir('backups', 0777, true);
}

// Utilizando a classe para gerar um backup na pasta 'backups'
// e manter os últimos dez arquivos
$backup = new BackupDatabase('backups', 10);
$backup->setDatabase('localhost', 'bancodados', 'bancodados', 'p3dE3IiUwcY7rxBGFlWXd5Fy9wHSPDVV');
$backup->generate();